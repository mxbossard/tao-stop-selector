# tao-stop-selector

## Installation ?
```
```

## Intialisation du projet
```
vue create tao-stop-selector
cd tao-stop-selector
git init
git remote add origin git@framagit.org:mxbossard/tao-stop-selector.git
git push --set-upstream origin master
npm run serve
```
Visitez http://localhost:8081/ 

## Dev
```
npm install vue-suggestion
```

## API TAO
Accès ici : http://94.143.218.36/?module=BTAccessTest

## Gitlab CI deploy on pages
1. Ajoutez le fichier de config [.gitlab-ci.yml](https://framagit.org/mxbossard/tao-stop-selector/-/blob/master/.gitlab-ci.yml)
2. Configurez la visibilité du dépot à "Public" dans settings > general > visibility

L'app est déployée ici après un commit : https://mxbossard.frama.io/tao-stop-selector

## Project setup (initialisé par vue create)
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
